/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.universi;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Process;
import android.view.Menu;

import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;

import androidx.fragment.app.Fragment;
import androidx.test.annotation.UiThreadTest;
import androidx.test.rule.ActivityTestRule;
import universum.studios.android.fragment.BackPressWatcher;
import universum.studios.android.fragment.annotation.ActionBarOptions;
import universum.studios.android.fragment.annotation.ContentView;
import universum.studios.android.fragment.annotation.FragmentAnnotations;
import universum.studios.android.test.AndroidTestCase;
import universum.studios.android.transition.BaseNavigationalTransition;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyNoMoreInteractions;

/**
 * @author Martin Albedinsky
 */
public final class UniversiActivityTest extends AndroidTestCase {
    
	@Rule public ActivityTestRule<TestActivity> ACTIVITY_RULE = new ActivityTestRule<>(TestActivity.class);

	@Override public void beforeTest() {
		super.beforeTest();
		// Ensure that we have always annotations processing enabled.
		FragmentAnnotations.setEnabled(true);
	}

	@Test public void testOnCreateOptionsMenu() {
		// Arrange:
		final TestActivity activity = ACTIVITY_RULE.getActivity();
		final Menu mockMenu = mock(Menu.class);
		// Act:
		activity.onCreateOptionsMenu(mockMenu);
		// Assert:
		verifyNoMoreInteractions(mockMenu);
	}

	@Test public void testGetFragmentControllerDefault() {
		// Arrange:
		final TestActivity activity = ACTIVITY_RULE.getActivity();
		// Act + Assert:
		assertThat(activity.getFragmentController(), is(notNullValue()));
	}

	@Ignore("Assertion is not valid!")
	@Test public void testRequestBindDataFromBackgroundThread() throws Throwable {
		// Arrange:
		final TestActivity activity = ACTIVITY_RULE.getActivity();
		// Act:
		activity.requestBindData();
		Thread.sleep(350);
		// Assert:
		// fixme: this assertion does not pass
		assertThat(activity.onBindDataInvoked, is(true));
	}

	@Test public void testCheckSelfPermission() {
		// Arrange:
		final TestActivity activity = ACTIVITY_RULE.getActivity();
		// Act + Assert:
		assertThat(
				activity.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE),
				is(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ?
						context().checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, Process.myPid(), Process.myUid()) :
						PackageManager.PERMISSION_GRANTED
				)
		);
	}

	@UiThreadTest
	@Test public void testShouldShowRequestPermissionRationale() {
		// Arrange:
		final TestActivity activity = ACTIVITY_RULE.getActivity();
		// Act + Assert:
		assertThat(activity.shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE), is(false));
	}

	@Test public void testFinishWithNavigationalTransition() {
		// Arrange:
		final TestActivity activity = ACTIVITY_RULE.getActivity();
		activity.setNavigationalTransition(new BaseNavigationalTransition() {});
		// Act + Assert:
		assertThat(activity.finishWithNavigationalTransition(), is(true));
		assertThat(activity.isFinishing(), is(true));
	}

	@Test public void testFinishWithNavigationalTransitionWhenThereIsNoTransition() {
		// Arrange:
		final TestActivity activity = ACTIVITY_RULE.getActivity();
		// Act + Assert:
		assertThat(activity.finishWithNavigationalTransition(), is(false));
		assertThat(activity.isFinishing(), is(true));
	}

	@Test public void testDestroy() {
		// Arrange:
		final TestActivity activity = ACTIVITY_RULE.getActivity();
		final UniversiActivityDelegate mockDelegate = mock(UniversiActivityDelegate.class);
		activity.setContextDelegate(mockDelegate);
		// Act:
		activity.finish();
		// Assert:
		assertThat(mockDelegate.isDestroyed(), is(true));
	}

	@ActionBarOptions(
			homeAsUp = ActionBarOptions.HOME_AS_UP_ENABLED,
			homeAsUpIndicator = android.R.drawable.ic_delete
	)
	@ContentView(android.R.layout.simple_list_item_1)
	public static final class TestActivity extends UniversiActivity {

		boolean onBindViewInvoked, onBindDataInvoked;

		@Override protected void onBindView() {
			super.onBindView();
			this.onBindViewInvoked = true;
		}

		@Override protected void onBindData() {
			super.onBindData();
			this.onBindDataInvoked = true;
		}
	}

	public static abstract class TestBackPressWatcherFragment extends Fragment implements BackPressWatcher {}
}