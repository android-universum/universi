Universi-Fragment
===============

This module contains `Fragment` implementation providing features of the **Universi** context.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Auniversi/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Auniversi/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:universi-fragment:${DESIRED_VERSION}@aar"

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [UniversiFragment](https://bitbucket.org/android-universum/universi/src/main/library-fragment/src/main/java/universum/studios/android/universi/UniversiFragment.java)