/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.universi;

import android.app.Application;

import org.junit.Test;
import org.robolectric.Robolectric;
import org.robolectric.android.controller.ActivityController;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import universum.studios.android.fragment.manage.FragmentController;
import universum.studios.android.fragment.manage.FragmentFactory;
import universum.studios.android.test.AndroidTestCase;
import universum.studios.android.test.TestActivity;
import universum.studios.android.transition.BaseNavigationalTransition;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.clearInvocations;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * @author Martin Albedinsky
 */
public final class UniversiActivityDelegateTest extends AndroidTestCase {

	@Test public void testInstantiation() {
		// Act:
		final FragmentActivity mockActivity = mockActivity();
		final UniversiActivityDelegate delegate = new UniversiActivityDelegate(mockActivity);
		// Act:
		assertThat(delegate.requireContext(), is(mockActivity));
	}

	@Test public void testActivityLifecycleResume() {
		// Arrange:
		final ActivityController<TestActivity> activityController = Robolectric.buildActivity(TestActivity.class).create();
		final UniversiActivityDelegate delegate = new UniversiActivityDelegate(activityController.get());
		activityController.start();
		delegate.setPaused(true);
		// Act:
		activityController.resume();
		// Assert:
		assertThat(delegate.isViewCreated(), is(false));
		assertThat(delegate.isPaused(), is(false));
	}

	@Test public void testActivityLifecyclePause() {
		// Arrange:
		final ActivityController<TestActivity> activityController = Robolectric.buildActivity(TestActivity.class).create();
		final UniversiActivityDelegate delegate = new UniversiActivityDelegate(activityController.get());
		activityController.start().resume();
		// Act:
		activityController.pause();
		// Assert:
		assertThat(delegate.isViewCreated(), is(false));
		assertThat(delegate.isPaused(), is(true));
	}

	@Test public void testActivityLifecycleDestroy() {
		// Arrange:
		final ActivityController<TestActivity> activityController = Robolectric.buildActivity(TestActivity.class).create();
		final UniversiActivityDelegate delegate = new UniversiActivityDelegate(activityController.get());
		activityController.start().resume().pause().stop();
		// Act:
		activityController.destroy();
		// Assert:
		assertThat(delegate.isViewCreated(), is(false));
		assertThat(delegate.isPaused(), is(true));
	}

	@Test public void testCreateDialogController() {
		// Arrange:
		final FragmentActivity mockActivity = mockActivity();
		final UniversiActivityDelegate delegate = new UniversiActivityDelegate(mockActivity);
		// Act + Assert:
		assertThat(delegate.createDialogController(), is(notNullValue()));
	}

	@Test public void testNavigationalTransition() {
		// Arrange:
		final FragmentActivity mockActivity = mockActivity();
		final UniversiActivityDelegate delegate = new UniversiActivityDelegate(mockActivity);
		final BaseNavigationalTransition mockTransition = mock(BaseNavigationalTransition.class);
		// Act + Assert:
		delegate.setNavigationalTransition(mockTransition);
		assertThat(delegate.getNavigationalTransition(), is(mockTransition));
		verify(mockTransition).configureIncomingTransitions(mockActivity);
	}

	@Test public void testSetNullNavigationalTransition() {
		// Arrange:
		final FragmentActivity mockActivity = mockActivity();
		final UniversiActivityDelegate delegate = new UniversiActivityDelegate(mockActivity);
		// Act:
		delegate.setNavigationalTransition(null);
		// Assert:
		assertThat(delegate.getNavigationalTransition(), is(nullValue()));
	}

	@Test public void testFinishWithNavigationalTransition() {
		// Arrange:
		final FragmentActivity mockActivity = mockActivity();
		final UniversiActivityDelegate delegate = new UniversiActivityDelegate(mockActivity);
		final BaseNavigationalTransition mockTransition = mock(BaseNavigationalTransition.class);
		delegate.setNavigationalTransition(mockTransition);
		// Act + Assert:
		assertThat(delegate.finishWithNavigationalTransition(), is(true));
	}

	@Test public void testFinishWithNavigationalTransitionWithoutAttachedTransition() {
		// Arrange:
		final FragmentActivity mockActivity = mockActivity();
		final UniversiActivityDelegate delegate = new UniversiActivityDelegate(mockActivity);
		// Act + Assert:
		assertThat(delegate.finishWithNavigationalTransition(), is(false));
	}

	@Test public void testFragmentController() {
		// Arrange:
		final FragmentActivity mockActivity = mockActivity();
		when(mockActivity.getSupportFragmentManager()).thenReturn(mock(FragmentManager.class));
		final UniversiActivityDelegate delegate = new UniversiActivityDelegate(mockActivity);
		final FragmentFactory mockFactory = mock(FragmentFactory.class);
		final FragmentController mockController = mock(FragmentController.class);
		delegate.setFragmentFactory(mockFactory);
		// Act + Assert:
		delegate.setFragmentController(mockController);
		assertThat(delegate.getFragmentController(), is(mockController));
		verify(mockController).setFactory(mockFactory);
		verifyNoMoreInteractions(mockController);
	}

	@Test public void testSetNullFragmentController() {
		// Arrange:
		final FragmentActivity mockActivity = mockActivity();
		when(mockActivity.getSupportFragmentManager()).thenReturn(mock(FragmentManager.class));
		final UniversiActivityDelegate delegate = new UniversiActivityDelegate(mockActivity);
		// Act:
		delegate.setFragmentController(null);
		// Assert:
		assertThat(delegate.getFragmentController(), is(notNullValue()));
	}

	@Test public void testFragmentFactory() {
		// Arrange:
		final FragmentActivity mockActivity = mockActivity();
		when(mockActivity.getSupportFragmentManager()).thenReturn(mock(FragmentManager.class));
		final UniversiActivityDelegate delegate = new UniversiActivityDelegate(mockActivity);
		final FragmentFactory mockFactory = mock(FragmentFactory.class);
		// Act + Assert:
		delegate.setFragmentFactory(mockFactory);
		assertThat(delegate.getFragmentFactory(), is(mockFactory));
		verifyNoMoreInteractions(mockFactory);
	}

	@Test public void testFindCurrentFragment() {
		// Arrange:
		final FragmentController mockController = mock(FragmentController.class);
		final Fragment mockFragment = mock(Fragment.class);
		when(mockController.findCurrentFragment()).thenReturn(mockFragment);
		final FragmentActivity mockActivity = mockActivity();
		final UniversiActivityDelegate delegate = new UniversiActivityDelegate(mockActivity);
		delegate.setFragmentController(mockController);
		// Act + Assert:
		assertThat(delegate.findCurrentFragment(), is(mockFragment));
	}

	@Test public void testFindCurrentFragmentWithoutControllerInitialized() {
		// Arrange:
		final FragmentManager mockManager = mock(FragmentManager.class);
		when(mockManager.getBackStackEntryCount()).thenReturn(1);
		final FragmentActivity mockActivity = mockActivity();
		when(mockActivity.getSupportFragmentManager()).thenReturn(mockManager);
		final UniversiActivityDelegate delegate = new UniversiActivityDelegate(mockActivity);
		// Act + Assert:
		assertThat(delegate.findCurrentFragment(), is(nullValue()));
	}

	@Test public void testPopFragmentsBackStack() {
		// Arrange:
		final FragmentManager mockManager = mock(FragmentManager.class);
		when(mockManager.getBackStackEntryCount()).thenReturn(1);
		final FragmentActivity mockActivity = mockActivity();
		when(mockActivity.getSupportFragmentManager()).thenReturn(mockManager);
		final UniversiActivityDelegate delegate = new UniversiActivityDelegate(mockActivity);
		// Act + Assert:
		assertThat(delegate.popFragmentsBackStack(), is(true));
		verify(mockManager, times(1)).getBackStackEntryCount();
		verify(mockManager, times(1)).popBackStack();
		verifyNoMoreInteractions(mockManager);
	}

	@Test public void testPopFragmentsBackStackOnEmptyStack() {
		// Arrange:
		final FragmentManager mockManager = mock(FragmentManager.class);
		when(mockManager.getBackStackEntryCount()).thenReturn(0);
		final FragmentActivity mockActivity = mockActivity();
		when(mockActivity.getSupportFragmentManager()).thenReturn(mockManager);
		final UniversiActivityDelegate delegate = new UniversiActivityDelegate(mockActivity);
		// Act + Assert:
		assertThat(delegate.popFragmentsBackStack(), is(false));
		verify(mockManager).getBackStackEntryCount();
		verifyNoMoreInteractions(mockManager);
	}

	@Test public void testDestroy() {
		// Arrange:
		final FragmentActivity mockActivity = mockActivity();
		final Application mockApplication = mockActivity.getApplication();
		final UniversiActivityDelegate delegate = new UniversiActivityDelegate(mockActivity);
		clearInvocations(mockApplication);
		// Act:
		delegate.destroy();
		// Assert:
		verify(mockApplication).unregisterActivityLifecycleCallbacks(any(Application.ActivityLifecycleCallbacks.class));
		verifyNoMoreInteractions(mockApplication);
	}

	private FragmentActivity mockActivity() {
		final FragmentActivity activity = mock(FragmentActivity.class);
		final Application application = mock(Application.class);
		when(activity.getApplication()).thenReturn(application);
		return activity;
	}
}