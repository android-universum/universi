/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.universi;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.junit.Test;
import org.robolectric.Robolectric;
import org.robolectric.android.controller.ActivityController;

import javax.annotation.Nonnull;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import universum.studios.android.dialog.manage.DialogController;
import universum.studios.android.test.AndroidTestCase;
import universum.studios.android.test.TestActivity;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.clearInvocations;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * @author Martin Albedinsky
 */
public final class UniversiFragmentDelegateTest extends AndroidTestCase {

	@Test public void testInstantiation() {
		// Arrange:
		final Fragment mockFragment = mockFragment();
		final FragmentManager mockFragmentManager = mockFragment.getParentFragmentManager();
		final UniversiFragmentDelegate delegate = new UniversiFragmentDelegate(mockFragment);
		// Act + Assert:
		assertThat(delegate.requireContext(), is(mockFragment.requireContext()));
		verify(mockFragmentManager).registerFragmentLifecycleCallbacks(any(FragmentManager.FragmentLifecycleCallbacks.class), eq(false));
		verifyNoMoreInteractions(mockFragmentManager);
	}

	@Test public void testFragmentLifecycleResume() {
		// Arrange:
		final ActivityController<TestActivity> activityController = Robolectric.buildActivity(TestActivity.class).create();
		final Fragment fragment = new TestFragment();
		activityController.get().getSupportFragmentManager().beginTransaction().add(fragment, "TAG").commitNow();
		final UniversiFragmentDelegate delegate = new UniversiFragmentDelegate(fragment);
		activityController.start();
		delegate.setPaused(true);
		// Act:
		activityController.resume();
		// Assert:
		assertThat(delegate.isViewCreated(), is(false));
		assertThat(delegate.isPaused(), is(false));
	}

	@Test public void testFragmentLifecyclePause() {
		// Arrange:
		final ActivityController<TestActivity> activityController = Robolectric.buildActivity(TestActivity.class).create();
		final Fragment fragment = new TestFragment();
		activityController.get().getSupportFragmentManager().beginTransaction().add(fragment, "TAG").commitNow();
		final UniversiFragmentDelegate delegate = new UniversiFragmentDelegate(fragment);
		activityController.start().resume();
		// Act:
		activityController.pause();
		// Assert:
		assertThat(delegate.isViewCreated(), is(false));
		assertThat(delegate.isPaused(), is(true));
	}

	@Test public void testFragmentLifecycleDestroy() {
		// Arrange:
		final ActivityController<TestActivity> activityController = Robolectric.buildActivity(TestActivity.class).create();
		final Fragment fragment = new TestFragment();
		activityController.get().getSupportFragmentManager().beginTransaction().add(fragment, "TAG").commitNow();
		final UniversiFragmentDelegate delegate = new UniversiFragmentDelegate(fragment);
		activityController.start().resume().pause().stop();
		// Act:
		activityController.destroy();
		// Assert:
		assertThat(delegate.isViewCreated(), is(false));
		assertThat(delegate.isPaused(), is(true));
	}

	@Test public void testCreateDialogController() {
		// Arrange:
		final Fragment mockFragment = mockFragment();
		final UniversiFragmentDelegate delegate = new UniversiFragmentDelegate(mockFragment);
		// Act:
		final DialogController controller = delegate.createDialogController();
		// Assert:
		assertThat(controller, is(notNullValue()));
		assertThat(controller, is(not(delegate.createDialogController())));
	}

	@Test public void testDestroy() {
		// Arrange:
		final Fragment mockFragment = mockFragment();
		final FragmentManager mockFragmentManager = mockFragment.getParentFragmentManager();
		final UniversiFragmentDelegate delegate = new UniversiFragmentDelegate(mockFragment);
		clearInvocations(mockFragmentManager);
		// Act:
		delegate.destroy();
		// Assert:
		verify(mockFragmentManager).unregisterFragmentLifecycleCallbacks(any(FragmentManager.FragmentLifecycleCallbacks.class));
		verifyNoMoreInteractions(mockFragmentManager);
	}

	private Fragment mockFragment() {
		final Context context = mock(Context.class);
		final Fragment fragment = mock(Fragment.class);
		when(fragment.requireContext()).thenReturn(context);
		final FragmentManager fragmentManager = mock(FragmentManager.class);
		when(fragment.getParentFragmentManager()).thenReturn(fragmentManager);
		return fragment;
	}

	public static final class TestFragment extends Fragment {

		@Override @Nonnull public View onCreateView(
				@NonNull final LayoutInflater inflater,
				@Nullable final ViewGroup container,
				@Nullable final Bundle savedInstanceState
		) {
			return new View(inflater.getContext());
		}
	}
}