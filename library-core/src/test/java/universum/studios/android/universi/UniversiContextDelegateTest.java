/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.universi;

import android.content.Context;

import org.junit.Test;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import universum.studios.android.dialog.manage.DialogController;
import universum.studios.android.dialog.manage.DialogFactory;
import universum.studios.android.dialog.manage.DialogXmlFactory;
import universum.studios.android.test.AndroidTestCase;
import universum.studios.android.test.TestActivity;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * @author Martin Albedinsky
 */
public final class UniversiContextDelegateTest extends AndroidTestCase {

	private static final int XML_DIALOGS_SET_RESOURCE_ID = 1;
	private static final int XML_DIALOG_RESOURCE_ID = 2;

	@Test public void testInstantiation() {
		// Act:
		final FragmentActivity mockActivity = mock(FragmentActivity.class);
		final UniversiContextDelegate delegate = new TestDelegate(mockActivity);
		// Assert:
		assertThat(delegate.requireContext(), is(mockActivity));
		assertThat(delegate.isViewCreated(), is(false));
		assertThat(delegate.isPaused(), is(false));
	}

	@Test public void testDialogController() {
		// Arrange:
		final UniversiContextDelegate delegate = new TestDelegate();
		final DialogController mockController = mock(DialogController.class);
		final DialogFactory mockFactory = mock(DialogFactory.class);
		// Act + Assert:
		delegate.setDialogFactory(mockFactory);
		delegate.setDialogController(mockController);
		assertThat(delegate.getDialogController(), is(mockController));
		verify(mockController).setFactory(mockFactory);
		verifyNoMoreInteractions(mockController);
	}

	@Test public void testSetNullDialogController() {
		// Arrange:
		final UniversiContextDelegate delegate = new TestDelegate();
		// Act:
		delegate.setDialogController(null);
		// Assert:
		assertThat(delegate.getDialogController(), is(notNullValue()));
	}

	@Test public void testDialogFactory() {
		// Arrange:
		final UniversiContextDelegate delegate = new TestDelegate();
		final DialogFactory mockFactory = mock(DialogFactory.class);
		// Act + Assert:
		delegate.setDialogFactory(mockFactory);
		assertThat(delegate.getDialogFactory(), is(mockFactory));
	}

	@Test public void testDialogXmlFactory() {
		// Arrange:
		final UniversiContextDelegate delegate = new TestDelegate();
		// Act:
		delegate.setDialogXmlFactory(XML_DIALOGS_SET_RESOURCE_ID);
		// Assert:
		assertThat(delegate.getDialogFactory(), is(notNullValue()));
		assertThat(delegate.getDialogFactory(), instanceOf(DialogXmlFactory.class));
	}

	@Test public void testSetDialogXmlFactoryWithZeroResource() {
		// Arrange:
		final UniversiContextDelegate delegate = new TestDelegate();
		delegate.setDialogFactory(mock(DialogFactory.class));
		// Act:
		delegate.setDialogXmlFactory(0);
		// Assert:
		assertThat(delegate.getDialogFactory(), is(nullValue()));
	}

	@Test public void testShowDialogWithId() {
		// Arrange:
		final FragmentActivity mockActivity = mock(FragmentActivity.class);
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final DialogFragment mockDialogFragment = mock(DialogFragment.class);
		when(mockActivity.getSupportFragmentManager()).thenReturn(mockFragmentManager);
		final UniversiContextDelegate delegate = new TestDelegate(mockActivity);
		final DialogFactory mockFactory = mock(DialogFactory.class);
		when(mockFactory.isDialogProvided(1)).thenReturn(true);
		when(mockFactory.createDialogTag(1)).thenReturn("Dialog.TAG.1");
		when(mockFactory.createDialog(1, null)).thenReturn(mockDialogFragment);
		delegate.setDialogFactory(mockFactory);
		// Act + Assert:
		assertThat(delegate.showDialogWithId(1, null), is(true));
		verify(mockFactory).isDialogProvided(1);
		verify(mockFactory).createDialogTag(1);
		verify(mockFactory).createDialog(1, null);
		verifyNoMoreInteractions(mockFactory);
	}

	@Test public void testShowDialogWithIdWithoutDialogFactoryAttached() {
		// Arrange:
		final FragmentActivity mockActivity = mock(FragmentActivity.class);
		final UniversiContextDelegate delegate = new TestDelegate(mockActivity);
		// Act + Assert:
		assertThat(delegate.showDialogWithId(1, null), is(false));
	}

	@Test public void testDismissDialogWithId() {
		// Arrange:
		final FragmentActivity mockActivity = mock(FragmentActivity.class);
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final DialogFragment mockDialogFragment = mock(DialogFragment.class);
		when(mockFragmentManager.findFragmentByTag("Dialog.TAG.1")).thenReturn(mockDialogFragment);
		when(mockActivity.getSupportFragmentManager()).thenReturn(mockFragmentManager);
		final UniversiContextDelegate delegate = new TestDelegate(mockActivity);
		final DialogFactory mockFactory = mock(DialogFactory.class);
		when(mockFactory.isDialogProvided(1)).thenReturn(true);
		when(mockFactory.createDialogTag(1)).thenReturn("Dialog.TAG.1");
		delegate.setDialogFactory(mockFactory);
		// Act + Assert:
		assertThat(delegate.dismissDialogWithId(1), is(true));
		verify(mockFactory, times(2)).isDialogProvided(1);
		verify(mockFactory, times(2)).createDialogTag(1);
		verifyNoMoreInteractions(mockFactory);
	}

	@Test public void testDismissDialogWithIdWhichIsNotShowing() {
		// Arrange:
		final FragmentActivity mockActivity = mock(FragmentActivity.class);
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		when(mockFragmentManager.findFragmentByTag("Dialog.TAG.1")).thenReturn(null);
		when(mockActivity.getSupportFragmentManager()).thenReturn(mockFragmentManager);
		final UniversiContextDelegate delegate = new TestDelegate(mockActivity);
		final DialogFactory mockFactory = mock(DialogFactory.class);
		when(mockFactory.isDialogProvided(1)).thenReturn(true);
		when(mockFactory.createDialogTag(1)).thenReturn("Dialog.TAG.1");
		delegate.setDialogFactory(mockFactory);
		// Act + Assert:
		assertThat(delegate.dismissDialogWithId(1), is(false));
		verify(mockFactory, times(2)).isDialogProvided(1);
		verify(mockFactory).createDialogTag(1);
		verifyNoMoreInteractions(mockFactory);
	}

	@Test public void testDismissDialogWithIdWithoutDialogFactoryAttached() {
		// Arrange:
		final FragmentActivity mockActivity = mock(FragmentActivity.class);
		final UniversiContextDelegate delegate = new TestDelegate(mockActivity);
		// Act + Assert:
		assertThat(delegate.dismissDialogWithId(1), is(false));
	}

	@Test public void testShowXmlDialog() {
		// Ignored, because we would need to import whole dialogs library in order to successfully
		// perform this test.
	}

	@Test public void testDismissXmlDialog() {
		// Arrange:
		final int dialogResource = XML_DIALOG_RESOURCE_ID;
		final FragmentActivity mockActivity = mock(FragmentActivity.class);
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final DialogFragment mockDialogFragment = mock(DialogFragment.class);
		when(mockFragmentManager.findFragmentByTag(DialogXmlFactory.class.getName() + ".TAG." + dialogResource)).thenReturn(mockDialogFragment);
		when(mockActivity.getSupportFragmentManager()).thenReturn(mockFragmentManager);
		final UniversiContextDelegate delegate = new TestDelegate(mockActivity);
		// Act + Assert:
		assertThat(delegate.dismissXmlDialog(dialogResource), is(true));
	}

	@Test public void testDismissXmlDialogWhichIsNotShowing() {
		// Arrange:
		final int dialogResource = XML_DIALOG_RESOURCE_ID;
		final FragmentActivity mockActivity = mock(FragmentActivity.class);
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		when(mockFragmentManager.findFragmentByTag(DialogXmlFactory.class.getName() + ".TAG." + dialogResource)).thenReturn(null);
		when(mockActivity.getSupportFragmentManager()).thenReturn(mockFragmentManager);
		final UniversiContextDelegate delegate = new TestDelegate(mockActivity);
		// Act + Assert:
		assertThat(delegate.dismissXmlDialog(dialogResource), is(false));
	}

	@Test public void testDismissXmlDialogWhichIsNotDialogFragment() {
		// Arrange:
		final int dialogResource = XML_DIALOG_RESOURCE_ID;
		final FragmentActivity mockActivity = mock(FragmentActivity.class);
		final FragmentManager mockFragmentManager = mock(FragmentManager.class);
		final Fragment mockFragment = mock(Fragment.class);
		when(mockFragmentManager.findFragmentByTag(DialogXmlFactory.class.getName() + ".TAG." + dialogResource)).thenReturn(mockFragment);
		when(mockActivity.getSupportFragmentManager()).thenReturn(mockFragmentManager);
		final UniversiContextDelegate delegate = new TestDelegate(mockActivity);
		// Act + Assert:
		assertThat(delegate.dismissXmlDialog(dialogResource), is(false));
	}

	@Test public void testIsViewCreated() {
		// Arrange:
		final FragmentActivity mockActivity = mock(FragmentActivity.class);
		final UniversiContextDelegate delegate = new TestDelegate(mockActivity);
		// Act + Assert:
		delegate.setViewCreated(true);
		assertThat(delegate.isViewCreated(), is(true));
		delegate.setViewCreated(false);
		assertThat(delegate.isViewCreated(), is(false));
	}

	@Test public void testIsPaused() {
		// Arrange:
		final FragmentActivity mockActivity = mock(FragmentActivity.class);
		final UniversiContextDelegate delegate = new TestDelegate(mockActivity);
		delegate.setPaused(true);
		// Act + Assert:
		assertThat(delegate.isPaused(), is(true));
		delegate.setPaused(false);
		assertThat(delegate.isPaused(), is(false));
	}

	@Test public void testRegisterRequest() {
		// Arrange:
		final FragmentActivity mockActivity = mock(FragmentActivity.class);
		final UniversiContextDelegate delegate = new TestDelegate(mockActivity);
		delegate.registerRequest(0x00000001);
		// Act + Assert:
		assertThat(delegate.isRequestRegistered(0x00000001), is(true));
	}

	@Test public void testUnregisterRequest() {
		// Arrange:
		final FragmentActivity mockActivity = mock(FragmentActivity.class);
		final UniversiContextDelegate delegate = new TestDelegate(mockActivity);
		delegate.registerRequest(0x00000001);
		delegate.unregisterRequest(0x00000001);
		// Act + Assert:
		assertThat(delegate.isRequestRegistered(0x00000001), is(false));
	}

	@Test public void testDestroy() {
		// Arrange:
		final FragmentActivity mockActivity = mock(FragmentActivity.class);
		final TestDelegate delegate = new TestDelegate(mockActivity);
		// Act + Assert:
		delegate.destroy();
		assertThat(delegate.isDestroyed(), is(true));
		assertThat(delegate.destroyCalledTimes, is(1));
		delegate.destroy();
		assertThat(delegate.destroyCalledTimes, is(1));
	}

	private static class TestDelegate extends UniversiContextDelegate {

		private FragmentActivity activity;
		int destroyCalledTimes = 0;

		TestDelegate() {
			this(mock(TestActivity.class));
		}

		TestDelegate(final FragmentActivity activity) {
			this.activity = activity;
		}

		@Override @NonNull protected Context requireContext() {
			return activity;
		}

		@Override protected @NonNull DialogController createDialogController() {
			return DialogController.create(activity);
		}

		@Override protected void onDestroy() {
			super.onDestroy();
			this.destroyCalledTimes++;
		}
	}
}