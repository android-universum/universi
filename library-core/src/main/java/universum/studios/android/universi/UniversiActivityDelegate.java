/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.universi;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import universum.studios.android.dialog.manage.DialogController;
import universum.studios.android.fragment.manage.FragmentController;
import universum.studios.android.fragment.manage.FragmentFactory;
import universum.studios.android.transition.BaseNavigationalTransition;

/**
 * An {@link UniversiContextDelegate} implementation that can be used within context of {@link Activity}.
 * Activity delegate has additional support for {@link Fragment Fragments} management via {@link FragmentController}.
 * Fragment controller can be accessed via {@link #getFragmentController()} and custom controller can
 * be specified via {@link #setFragmentController(FragmentController)}. Fragment factory that provides
 * fragments to be displayed in a context of a specific UniversiActivity can be specified via
 * {@link #setFragmentFactory(FragmentFactory)}.
 * <p>
 * Navigational transition that can be specified via {@link #setNavigationalTransition(BaseNavigationalTransition)}
 * can be used to finish the associated activity.
 * <p>
 * <b>Note</b> that this class has not been made final on purpose so it may be easily mocked in tests,
 * thought it should not been extended.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @see UniversiFragmentDelegate
 */
@SuppressWarnings("WeakerAccess")
public class UniversiActivityDelegate extends UniversiContextDelegate {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "UniversiActivityDelegate";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Activity instance for which has been this delegate created via {@link #create(FragmentActivity)}.
	 */
	private final FragmentActivity activity;

	/**
	 * Callbacks used to monitor lifecycle of the associated activity.
	 */
	private final Application.ActivityLifecycleCallbacks lifecycleCallbacks = new Application.ActivityLifecycleCallbacks() {

		/**
		 */
		@Override public void onActivityCreated(@NonNull final Activity activity, @Nullable final Bundle savedInstanceState) {}

		/**
		 */
		@Override public void onActivityStarted(@NonNull final Activity activity) {}

		/**
		 */
		@Override public void onActivityResumed(@NonNull final Activity activity) {
			if (UniversiActivityDelegate.this.activity == activity) {
				setPaused(false);
			}
		}

		/**
		 */
		@Override public void onActivityPaused(@NonNull final Activity activity) {
			if (UniversiActivityDelegate.this.activity == activity) {
				setPaused(true);
			}
		}

		/**
		 */
		@Override public void onActivityStopped(@NonNull final Activity activity) {}

		/**
		 */
		@Override public void onActivitySaveInstanceState(@NonNull final Activity activity, @NonNull final Bundle outState) {}

		/**
		 */
		@Override public void onActivityDestroyed(@NonNull final Activity activity) {
			if (UniversiActivityDelegate.this.activity == activity) {
				setViewCreated(false);
			}
		}
	};

	/**
	 * Controller that is used to show and hide fragments within the associated activity context.
	 */
	private FragmentController fragmentController;

	/**
	 * Factory providing fragment instances for the {@link #fragmentController}.
	 */
	private FragmentFactory fragmentFactory;

	/**
	 * Navigational transition that can be used to finish the associated activity context.
	 */
	private BaseNavigationalTransition navigationalTransition;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of UniversiActivityDelegate for the given <var>activity</var>.
	 */
	@VisibleForTesting UniversiActivityDelegate(@NonNull final FragmentActivity activity) {
		this.activity = activity;
		this.activity.getApplication().registerActivityLifecycleCallbacks(lifecycleCallbacks);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Creates a new instance of UniversiActivityDelegate for the given <var>activity</var>.
	 * <p>
	 * <b>Do not forget to destroy the new delegate via {@link #destroy()} when the activity is
	 * also destroyed.</b>
	 *
	 * @param activity The activity context in which will be the new delegate used.
	 * @return Ready to be used delegate.
	 */
	@NonNull public static UniversiActivityDelegate create(@NonNull final FragmentActivity activity) {
		return new UniversiActivityDelegate(activity);
	}

	/**
	 */
	@Override @NonNull protected Context requireContext() {
		return activity;
	}

	/**
	 */
	@Override @NonNull final protected DialogController createDialogController() {
		return DialogController.create(activity);
	}

	/**
	 * Sets a navigational transition that can be used to finish associated activity via
	 * {@link #finishWithNavigationalTransition()}.
	 * <p>
	 * If the given transition is not {@code null} this method will also configure incoming transitions
	 * for the associated activity via {@link BaseNavigationalTransition#configureIncomingTransitions(Activity)}.
	 *
	 * @param transition The desired transition. May be {@code null} to clear the current one.
	 *
	 * @see #getNavigationalTransition()
	 */
	public void setNavigationalTransition(@Nullable final BaseNavigationalTransition transition) {
		this.navigationalTransition = transition;
		if (transition != null) {
			transition.configureIncomingTransitions(activity);
		}
	}

	/**
	 * Returns the navigational transition used to finish associated activity via
	 * {@link #finishWithNavigationalTransition()}.
	 *
	 * @return Attached navigational transition or {@code null} if no transition has been specified.
	 *
	 * @see #setNavigationalTransition(BaseNavigationalTransition)
	 */
	@Nullable public BaseNavigationalTransition getNavigationalTransition() {
		return navigationalTransition;
	}

	/**
	 * Finishes the associated activity with navigational transition specified via
	 * {@link #setNavigationalTransition(BaseNavigationalTransition)} (if any).
	 *
	 * @return {@code True} if transition has been started, {@code false} otherwise.
	 */
	public boolean finishWithNavigationalTransition() {
		if (navigationalTransition == null) {
			return false;
		}
		this.navigationalTransition.finish(activity);
		return true;
	}

	/**
	 * Sets a controller used to manage (show/hide) fragments in context of the associated activity.
	 *
	 * @param controller The desired controller. May be {@code null} to use the default one.
	 *
	 * @see #getFragmentController()
	 * @see #setFragmentFactory(FragmentFactory)
	 */
	public void setFragmentController(@Nullable final FragmentController controller) {
		this.fragmentController = controller;
		if (fragmentFactory != null) {
			this.ensureFragmentController();
			this.fragmentController.setFactory(fragmentFactory);
		}
	}

	/**
	 * Returns the default controller or the one specified via {@link #setFragmentController(FragmentController)}
	 * that is used to manage fragments of the associated activity.
	 *
	 * @return Fragment controller instance ready to show/hide fragment instances.
	 */
	@NonNull public FragmentController getFragmentController() {
		this.ensureFragmentController();
		return fragmentController;
	}

	/**
	 * Sets a factory that provides fragment instances for fragment controller of this delegate.
	 *
	 * @param factory The desired factory. May be {@code null} to clear the current one.
	 *
	 * @see #setFragmentController(FragmentController)
	 * @see #getFragmentController()
	 * @see #getFragmentFactory()
	 */
	public void setFragmentFactory(@Nullable final FragmentFactory factory) {
		this.fragmentFactory = factory;
		this.ensureFragmentController();
		this.fragmentController.setFactory(factory);
	}

	/**
	 * Returns the factory providing fragment instances for fragment controller of this delegate.
	 *
	 * @return Instance of fragment factory or {@code null} if no factory has been specified.
	 *
	 * @see #setFragmentFactory(FragmentFactory)
	 */
	@Nullable public FragmentFactory getFragmentFactory() {
		return fragmentFactory;
	}

	/**
	 * Ensures that the fragment controller is initialized.
	 */
	private void ensureFragmentController() {
		if (fragmentController == null) this.fragmentController = FragmentController.create(activity);
	}

	/**
	 * Searches for current fragment displayed in container that is used by {@link FragmentController}
	 * of this delegate to show fragments in the context of the associated activity.
	 *
	 * @return Instance of current fragment found via {@link FragmentController#findCurrentFragment()}
	 * or {@code null} if there is no fragment displayed.
	 */
	@Nullable public Fragment findCurrentFragment() {
		return fragmentController == null ? null : fragmentController.findCurrentFragment();
	}

	/**
	 * Pops stack with fragments of the associated activity via {@link FragmentManager#popBackStack()}
	 *
	 * @return {@code True} if the stack has ben popped, {@code false} otherwise.
	 */
	public boolean popFragmentsBackStack() {
		final FragmentManager fragmentManager = activity.getSupportFragmentManager();
		if (fragmentManager.getBackStackEntryCount() > 0) {
			fragmentManager.popBackStack();
			return true;
		}
		return false;
	}

	/**
	 */
	@Override protected void onDestroy() {
		super.onDestroy();
		this.activity.getApplication().unregisterActivityLifecycleCallbacks(lifecycleCallbacks);
		if (fragmentController != null) {
			this.fragmentController.destroy();
			this.fragmentController = null;
		}
	}

	/*
	 * Inner classes ===============================================================================
	 */
}