/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.universi;

import android.content.Context;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.XmlRes;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import universum.studios.android.dialog.DialogOptions;
import universum.studios.android.dialog.XmlDialog;
import universum.studios.android.dialog.manage.DialogController;
import universum.studios.android.dialog.manage.DialogFactory;
import universum.studios.android.dialog.manage.DialogRequest;
import universum.studios.android.dialog.manage.DialogXmlFactory;

/**
 * Context delegate of which implementations are used by {@link UniversiActivity}, {@link UniversiCompatActivity}
 * and {@link UniversiActivity} to provide features in unified 'fashion' regardless of context in
 * which are these features supported.
 * <p>
 * A the current version this context delegate supports features described below:
 *
 * <h3>1) {@link DialogFragment DialogFragments} management</h3>
 * This feature is supported using {@link DialogController}. This controller can be accessed via
 * {@link #getDialogController()} and custom controller can be specified via {@link #setDialogController(DialogController)}.
 * <p>
 * Without any set up this delegate can be used to show any implementation of {@link XmlDialog} via
 * {@link #showXmlDialog(int, DialogOptions)} where for such case will be created internal instance
 * of {@link DialogXmlFactory} used to inflate such dialogs from an Xml file containing a
 * <b>single dialog entry</b>.
 * <p>
 * If the associated context wants to display XmlDialogs from an Xml file containing a
 * <b>set of multiple dialog entries</b> or just simple dialogs instantiated via plain Java code it
 * need to set instance of {@link DialogFactory} that can provide that dialogs
 * via {@link #setDialogFactory(DialogFactory)}. Such dialogs can be than displayed
 * via {@link #showDialogWithId(int, DialogOptions)}.
 * <p>
 * Already showing dialogs can be dismissed via {@link #dismissDialogWithId(int)} or {@link #dismissXmlDialog(int)}.
 * <p>
 * If the associated context is paused (indicated via {@link #setPaused(boolean)}), invoking one of
 * {@code showDialog(...)} methods will be ignored.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
@SuppressWarnings("WeakerAccess")
public abstract class UniversiContextDelegate {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "UniversiContextDelegate";

	/**
	 * Request flag indicating whether the wrapped context requested binding of its data to its view
	 * hierarchy or not.
	 */
	static final int REQUEST_BIND_DATA = 1;

	/**
	 * Flag indicating whether the associated context has its view created or not.
	 */
	private static final int FLAG_VIEW_CREATED = 1;

	/**
	 * Flag indicating whether the associated context is paused or not.
	 */
	private static final int FLAG_PAUSED = 1 << 1;

	/**
	 * Flag indicating whether the delegate is destroyed or not.
	 */
	private static final int FLAG_DESTROYED = 1 << 2;

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Set of private flags specified for this delegate.
	 */
	private int flags;

	/**
	 * Set of request flags.
	 */
	private int requestFlags;

	/**
	 * Controller that is used to show and dismiss dialogs within context that uses this delegate.
	 */
	private DialogController dialogController;

	/**
	 * Factory providing dialog instances for the {@link #dialogController}.
	 */
	private DialogFactory dialogFactory;

	/**
	 * Dialog factory used to inflate dialog instances presented within a single Xml file.
	 */
	private DialogXmlFactory dialogXmlFactory;

	/*
	 * Constructors ================================================================================
	 */

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Called to obtain context to be used by this delegate.
	 *
	 * @return The context ready to be used.
	 */
	@NonNull protected abstract Context requireContext();

	/**
	 * Sets a controller that should be used to show and dismiss dialogs within context that uses
	 * this delegate.
	 *
	 * @param controller The desired controller. May be {@code null} to use the default one.
	 *
	 * @see #getDialogController()
	 */
	public void setDialogController(@Nullable final DialogController controller) {
		this.dialogController = controller;
		if (dialogFactory != null) {
			this.ensureDialogController();
			this.dialogController.setFactory(dialogFactory);
		}
	}

	/**
	 * Returns the controller that can be used to show and dismiss dialogs within context that uses
	 * this delegate
	 * <p>
	 * If not specified, instance of {@link DialogController} is instantiated by default.
	 *
	 * @return The dialog controller of this delegate.
	 *
	 * @see #setDialogController(DialogController)
	 */
	@NonNull public DialogController getDialogController() {
		this.ensureDialogController();
		return dialogController;
	}

	/**
	 * Ensures that the dialog controller is initialized.
	 */
	private void ensureDialogController() {
		if (dialogController == null) {
			this.dialogController = createDialogController();
		}
	}

	/**
	 * Invoked to create a new instance of DialogController for the context that uses this delegate.
	 *
	 * @return New DialogController instance.
	 */
	@NonNull protected abstract DialogController createDialogController();

	/**
	 * Same as {@link #setDialogFactory(DialogFactory)} where will be passed instance of
	 * {@link DialogXmlFactory} with the specified <var>xmlDialogsSet</var>.
	 *
	 * @param xmlDialogsSet Resource id of the desired Xml file containing Xml dialogs that the
	 *                      factory should provide. May be {@code 0} to remove the current one.
	 */
	public void setDialogXmlFactory(@XmlRes final int xmlDialogsSet) {
		setDialogFactory(xmlDialogsSet == 0 ? null : new DialogXmlFactory(requireContext(), xmlDialogsSet));
	}

	/**
	 * Specifies a factory that should provide dialog instances for {@link DialogController} of
	 * this delegate.
	 *
	 * @param factory The desired factory. May be {@code null} to remove the current one.
	 *
	 * @see #getDialogFactory()
	 * @see #showDialogWithId(int, DialogOptions)
	 */
	public void setDialogFactory(@Nullable final DialogFactory factory) {
		this.dialogFactory = factory;
		this.ensureDialogController();
		this.dialogController.setFactory(factory);
	}

	/**
	 * Returns the current dialog factory specified for this delegate.
	 *
	 * @return Dialog factory or {@code null} if no factory has been specified yet.
	 *
	 * @see #setDialogFactory(DialogFactory)
	 */
	@Nullable public DialogFactory getDialogFactory() {
		return dialogFactory;
	}

	/**
	 * Shows a dialog that is provided by the current dialog factory under the specified <var>dialogId</var>.
	 *
	 * @param dialogId Id of the desired dialog to show.
	 * @param options  Options for the dialog.
	 * @return {@code True} if dialog has been shown, {@code false} if context that uses this delegate
	 * is currently <b>paused</b> or its <b>state has been already saved</b> or does not have its dialog
	 * factory specified.
	 *
	 * @see DialogController#newRequest(int)
	 * @see #setDialogFactory(DialogFactory)
	 * @see #dismissDialogWithId(int)
	 */
	public boolean showDialogWithId(final int dialogId, @Nullable final DialogOptions options) {
		if (dialogFactory == null) {
			return false;
		}
		this.ensureDialogController();
		return dialogController.newRequest(dialogId).options(options).execute() != null;
	}

	/**
	 * Dismisses a dialog that is provided by the current dialog factory under the specified <var>dialogId</var>.
	 *
	 * @param dialogId Id of the desired dialog to dismiss.
	 * @return {@code True} if dialog has been dismissed, {@code false} if context that uses this
	 * delegate is currently <b>paused</b> or does not have its dialog factory specified.
	 *
	 * @see DialogController#newRequest(int)
	 * @see #showDialogWithId(int, DialogOptions)
	 */
	public boolean dismissDialogWithId(final int dialogId) {
		if (dialogFactory == null) {
			return false;
		}
		this.ensureDialogController();
		return dialogController.newRequest(dialogId).intent(DialogRequest.DISMISS).allowStateLoss(true).execute() != null;
	}

	/**
	 * Like {@link #showDialogWithId(int, DialogOptions)}, but in this case will be used internal
	 * instance of {@link DialogXmlFactory} to create (inflate) the desired dialog instance to be
	 * shown.
	 *
	 * @param resId   Resource id of Xml file containing the desired dialog (its specification) to show.
	 * @param options Options for the dialog.
	 * @return {@code True} if dialog has been successfully inflated and shown, {@code false} if
	 * context that uses this delegate is currently <b>paused</b> or its <b>state has been already saved</b>
	 * or dialog failed to be inflated.
	 *
	 * @see DialogXmlFactory#createDialog(int, DialogOptions)
	 * @see #dismissXmlDialog(int)
	 */
	public boolean showXmlDialog(@XmlRes final int resId, @Nullable final DialogOptions options) {
		final DialogXmlFactory dialogFactory = accessDialogXmlFactory();
		final DialogFragment dialog = dialogFactory.createDialog(resId, options);
		if (dialog == null) {
			return false;
		}
		this.ensureDialogController();
		return dialogController.newRequest(dialog).tag(dialogFactory.createDialogTag(resId)).execute() != null;
	}

	/**
	 * Dismisses an Xml dialog that has been shown via {@link #showXmlDialog(int, DialogOptions)}.
	 *
	 * @param resId Resource id of Xml file containing the desired dialog (its specification) to dismiss.
	 * @return {@code True} if dialog has been dismissed, {@code false} if context that uses this
	 * delegate is currently <b>paused</b>.
	 *
	 * @see #showXmlDialog(int, DialogOptions)
	 */
	public boolean dismissXmlDialog(@XmlRes final int resId) {
		this.ensureDialogController();
		final Fragment fragment = dialogController.getFragmentManager().findFragmentByTag(accessDialogXmlFactory().createDialogTag(resId));
		return fragment instanceof DialogFragment && dialogController.newRequest((DialogFragment) fragment)
				.intent(DialogRequest.DISMISS)
				.allowStateLoss(true)
				.execute() != null;
	}

	/**
	 * Ensures that the dialog Xml factory is initialized and returns it. If {@link #dialogFactory}
	 * is instance of {@link DialogXmlFactory} than it will be returned.
	 *
	 * @return Instance of DialogXmlFactory that can be used to create instances of Xml dialogs.
	 */
	private DialogXmlFactory accessDialogXmlFactory() {
		if (dialogFactory instanceof DialogXmlFactory) {
			return (DialogXmlFactory) dialogFactory;
		}
		if (dialogXmlFactory == null) {
			this.dialogXmlFactory = new DialogXmlFactory(requireContext());
		}
		return dialogXmlFactory;
	}

	/**
	 * Sets a boolean flag indicating whether a view hierarchy of the associated context is created or not.
	 *
	 * @param created {@code True} if view hierarchy is created, {@code false} otherwise.
	 *
	 * @see #isViewCreated()
	 */
	public void setViewCreated(final boolean created) {
		this.updateFlags(FLAG_VIEW_CREATED, created);
	}

	/**
	 * Returns the boolean flag indicating whether view hierarchy of the associated context is created
	 * or not.
	 *
	 * @return {@code True} if view is created, {@code false} otherwise.
	 *
	 * @see #setViewCreated(boolean)
	 */
	public boolean isViewCreated() {
		return hasFlag(FLAG_VIEW_CREATED);
	}

	/**
	 * Sets a boolean flag indicating whether the associated context has been paused or not.
	 *
	 * @param paused {@code True} if context is paused, {@code false} otherwise.
	 *
	 * @see #isPaused() ()
	 */
	public void setPaused(final boolean paused) {
		this.updateFlags(FLAG_PAUSED, paused);
	}

	/**
	 * Returns the boolean flag indicating whether the related context is paused or not.
	 * <p>
	 * This should be {@code true} after context is paused and before it is again resumed.
	 *
	 * @return {@code True} if context is paused, {@code false} otherwise.
	 *
	 * @see #setPaused(boolean)
	 */
	public boolean isPaused() {
		return hasFlag(FLAG_PAUSED);
	}

	/**
	 * Adds a request with the specified <var>request</var> flag into the registered ones.
	 *
	 * @param requestFlag The request flag that should be registered.
	 *
	 * @see #isRequestRegistered(int)
	 * @see #unregisterRequest(int)
	 */
	public void registerRequest(final int requestFlag) {
		this.requestFlags |= requestFlag;
	}

	/**
	 * Removes request with the specified <var>request</var> flag from the registered ones.
	 *
	 * @param requestFlag The request flag that should be unregistered.
	 *
	 * @see #registerRequest(int)
	 * @see #isRequestRegistered(int)
	 */
	public void unregisterRequest(final int requestFlag) {
		this.requestFlags &= ~requestFlag;
	}

	/**
	 * Checks whether a request with the specified <var>request</var> flag is registered or not.
	 *
	 * @param requestFlag The request flag for which to check if it is registered.
	 * @return {@code True} if request is registered, {@code false} otherwise.
	 *
	 * @see #registerRequest(int)
	 * @see #unregisterRequest(int)
	 */
	public boolean isRequestRegistered(final int requestFlag) {
		return (requestFlags & requestFlag) != 0;
	}

	/**
	 * Destroys this delegate instance rendering it unavailable for further use.
	 * <p>
	 * Context delegate should be destroyed whenever its associated context is also being destroyed.
	 * <p>
	 * <b>Note that already destroyed delegate should not be used any further.</b>
	 */
	public final void destroy() {
		if (!hasFlag(FLAG_DESTROYED)) {
			onDestroy();
			this.updateFlags(FLAG_DESTROYED, true);
		}
	}

	/**
	 * Invoked whenever {@link #destroy()} is called for this delegate to perform its destroy operation
	 * while this delegate is not yet in destroyed state.
	 */
	@CallSuper protected void onDestroy() {
		if (dialogController != null) {
			this.dialogController.destroy();
			this.dialogController = null;
		}
	}

	/**
	 * Checks whether this delegate is destroyed.
	 *
	 * @return {@code True} if this delegate has been already destroyed, {@code false} otherwise.
	 *
	 * @see #destroy()
	 */
	public final boolean isDestroyed() {
		return hasFlag(FLAG_DESTROYED);
	}

	/**
	 * Updates the current private flags.
	 *
	 * @param flag Value of the desired flag to add/remove to/from the current private flags.
	 * @param add  Boolean flag indicating whether to add or remove the specified <var>flag</var>.
	 */
	private void updateFlags(final int flag, final boolean add) {
		if (add) this.flags |= flag;
		else this.flags &= ~flag;
	}

	/**
	 * Returns a boolean flag indicating whether the specified <var>flag</var> is contained within
	 * the current private flags or not.
	 *
	 * @param flag Value of the flag to check.
	 * @return {@code True} if the requested flag is contained, {@code false} otherwise.
	 */
	private boolean hasFlag(final int flag) {
		return (flags & flag) != 0;
	}

	/*
	 * Inner classes ===============================================================================
	 */
}