/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.universi;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import universum.studios.android.dialog.manage.DialogController;

/**
 * An {@link UniversiContextDelegate} implementation that can be used within context of {@link Fragment}.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @see UniversiActivityDelegate
 */
public class UniversiFragmentDelegate extends UniversiContextDelegate {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "UniversiFragmentDelegate";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Fragment instance for which has been this delegate created via {@link #create(Fragment)}.
	 */
	private final Fragment fragment;

	/**
	 * Callbacks used to monitor lifecycle of the associated fragment.
	 */
	private final FragmentManager.FragmentLifecycleCallbacks lifecycleCallbacks = new FragmentManager.FragmentLifecycleCallbacks() {

		/**
		 */
		@Override public void onFragmentResumed(@NonNull final FragmentManager manager, @NonNull final Fragment fragment) {
			super.onFragmentViewDestroyed(manager, fragment);
			if (UniversiFragmentDelegate.this.fragment == fragment) {
				setPaused(false);
			}
		}

		/**
		 */
		@Override public void onFragmentPaused(@NonNull final FragmentManager manager, @NonNull final Fragment fragment) {
			super.onFragmentViewDestroyed(manager, fragment);
			if (UniversiFragmentDelegate.this.fragment == fragment) {
				setPaused(true);
			}
		}

		/**
		 */
		@Override public void onFragmentViewDestroyed(@NonNull final FragmentManager manager, @NonNull final Fragment fragment) {
			super.onFragmentViewDestroyed(manager, fragment);
			if (UniversiFragmentDelegate.this.fragment == fragment) {
				setViewCreated(false);
			}
		}
	};

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of UniversiFragmentDelegate for the given <var>fragment</var>.
	 */
	@VisibleForTesting UniversiFragmentDelegate(@NonNull final Fragment fragment) {
		this.fragment = fragment;
		this.fragment.getParentFragmentManager().registerFragmentLifecycleCallbacks(lifecycleCallbacks, false);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Creates a new instance of UniversiFragmentDelegate for the given <var>fragment</var>.
	 * <p>
	 * <b>Do not forget to destroy the new delegate via {@link #destroy()} when the fragment is
	 * also destroyed.</b>
	 *
	 * @param fragment The fragment context in which will be the new delegate used.
	 * @return Ready to be used delegate.
	 */
	@NonNull public static UniversiFragmentDelegate create(@NonNull final Fragment fragment) {
		return new UniversiFragmentDelegate(fragment);
	}

	/**
	 */
	@Override @NonNull protected Context requireContext() {
		return fragment.requireContext();
	}

	/**
	 */
	@Override @NonNull final protected DialogController createDialogController() {
		return DialogController.create(fragment);
	}

	/**
	 */
	@Override protected void onDestroy() {
		super.onDestroy();
		this.fragment.getParentFragmentManager().unregisterFragmentLifecycleCallbacks(lifecycleCallbacks);
	}

	/*
	 * Inner classes ===============================================================================
	 */
}