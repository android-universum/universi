Change-Log
===============
> Regular configuration update: _09.03.2020_

More **detailed changelog** for each respective version may be viewed by pressing on a desired _version's name_.

## Version 1.x ##

### 1.3.0 ###
> 09.03.2020

- Regular **dependencies update** (mainly artifacts from **Android Jetpack**).
     - Updated `androidx.fragment:fragment` dependency to **1.2.2** version.
- Removed elements that has been **deprecated** in previous versions.
- Quality and stability improvements.

### [1.2.2](https://bitbucket.org/android-universum/universi/wiki/version/1.x) ###
> 30.12.2019

- Resolved [Issue #5](https://bitbucket.org/android-universum/universi/issues/5).

### [1.2.1](https://bitbucket.org/android-universum/universi/wiki/version/1.x) ###
> 08.12.2019

- Regular **dependencies update** (mainly artifacts from **Android Jetpack**).

### [1.2.0](https://bitbucket.org/android-universum/universi/wiki/version/1.x) ###
> 23.01.2019

- Updated to use [Fragments](https://bitbucket.org/android-universum/fragments) and
  [Dialogs](https://bitbucket.org/android-universum/dialogs) libraries in versions which use
  `Lifecycle` to ensure safe execution of `FragmentRequests`/`DialogRequests`.
- Small updates and improvements.

### [1.1.1](https://bitbucket.org/android-universum/universi/wiki/version/1.x) ###
> 27.12.2018

- Resolved [Issue #1](https://bitbucket.org/android-universum/universi/issues/1).

### [1.1.0](https://bitbucket.org/android-universum/universi/wiki/version/1.x) ###
> 21.11.2018

- Regular **dependencies update** (mainly to use new artifacts from **Android Jetpack**).

### [1.0.6](https://bitbucket.org/android-universum/universi/wiki/version/1.x) ###
> 24.07.2018

- Small updates.

### [1.0.5](https://bitbucket.org/android-universum/universi/wiki/version/1.x) ###
> 16.06.2018

- Small updates.

### [1.0.4](https://bitbucket.org/android-universum/universi/wiki/version/1.x) ###
> 20.12.2017

- Small updates and improvements.
- Updated **dependencies** versions and _Gradle_ configuration.

### [1.0.3](https://bitbucket.org/android-universum/universi/wiki/version/1.x) ###
> 06.06.2017

- Small patches and code quality improvements.

### [1.0.2](https://bitbucket.org/android-universum/universi/wiki/version/1.x) ###
> 14.05.2017

- Updated [Fragments](https://bitbucket.org/android-universum/fragments) library dependency to
  the **1.2.0** version.
- Updated [Transitions](https://bitbucket.org/android-universum/transitions) library dependency
  to the **1.1.0** version.

### [1.0.1](https://bitbucket.org/android-universum/universi/wiki/version/1.x) ###
> 12.04.2017

- Stability improvements.

### [1.0.0](https://bitbucket.org/android-universum/universi/wiki/version/1.x) ###
> 02.04.2017

- First production release.