Android Universi
===============

[![CircleCI](https://circleci.com/bb/android-universum/universi.svg?style=shield)](https://circleci.com/bb/android-universum/universi)
[![Codecov](https://codecov.io/bb/android-universum/universi/branch/main/graph/badge.svg)](https://codecov.io/bb/android-universum/universi)
[![Codacy](https://api.codacy.com/project/badge/Grade/e6a4ae8b36a94c6390d528192e331d39)](https://www.codacy.com/app/universum-studios/universi?utm_source=android-universum@bitbucket.org&amp;utm_medium=referral&amp;utm_content=android-universum/universi&amp;utm_campaign=Badge_Grade)
[![Android](https://img.shields.io/badge/android-9.0-blue.svg)](https://developer.android.com/about/versions/pie/android-9.0)
[![Robolectric](https://img.shields.io/badge/robolectric-4.3.1-blue.svg)](http://robolectric.org)
[![Android Jetpack](https://img.shields.io/badge/Android-Jetpack-brightgreen.svg)](https://developer.android.com/jetpack)

## ! OBSOLETE ! ##

**> This project has become obsolete and will be no longer maintained. <**

---

Simple universal library for the Android platform providing elements that may be used as base for Activities and Fragments in Android applications.

For more information please visit the **[Wiki](https://bitbucket.org/android-universum/universi/wiki)**.

## Download ##

Download the latest **[release](https://bitbucket.org/android-universum/universi/addon/pipelines/deployments "Deployments page")** or **add as dependency** in your project via:

### Gradle ###

    implementation "universum.studios.android:universi:${DESIRED_VERSION}@aar"

## Modules ##

This library may be used via **separate [modules](https://bitbucket.org/android-universum/universi/src/main/MODULES.md)**
in order to depend only on desired _parts of the library's code base_ what ultimately results in **fewer dependencies**.

## Compatibility ##

Supported down to the **Android [API Level 14](http://developer.android.com/about/versions/android-4.0.html "See API highlights")**.

### Dependencies ###

- [`androidx.annotation:annotation`](https://developer.android.com/jetpack/androidx)
- [`androidx.fragment:fragment`](https://developer.android.com/jetpack/androidx)
- [`androidx.appcompat:appcompat`](https://developer.android.com/jetpack/androidx)
- [`androidx.recyclerview:recyclerview`](https://developer.android.com/jetpack/androidx)
- [`universum.studios.android:transitions-navigational-base`](https://bitbucket.org/android-universum/transitions/src/main/MODULES.md)
- [`universum.studios.android:fragments-core`](https://bitbucket.org/android-universum/fragments/src/main/MODULES.md)
- [`universum.studios.android:fragments-base`](https://bitbucket.org/android-universum/fragments/src/main/MODULES.md)
- [`universum.studios.android:fragments-common`](https://bitbucket.org/android-universum/fragments/src/main/MODULES.md)
- [`universum.studios.android:fragments-manage-core`](https://bitbucket.org/android-universum/fragments/src/main/MODULES.md)
- [`universum.studios.android:dialogs-core`](https://bitbucket.org/android-universum/dialogs/src/main/MODULES.md)
- [`universum.studios.android:dialogs-manage`](https://bitbucket.org/android-universum/dialogs/src/main/MODULES.md)

## [License](https://bitbucket.org/android-universum/universi/src/main/LICENSE.md) ##

**Copyright 2020 Universum Studios**

_Licensed under the Apache License, Version 2.0 (the "License");_

You may not use this file except in compliance with the License. You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software distributed under the License
is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
or implied.
     
See the License for the specific language governing permissions and limitations under the License.